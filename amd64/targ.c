#include "all.h"

Amd64Op amd64_op[NOp] = {
#define O(op, t, x) [O##op] =
#define X(nm, zf, lf) { nm, zf, lf, },
	#include "../ops.h"
};

static int
amd64_memargs(int op)
{
	return amd64_op[op].nmem;
}

#define AMD64_COMMON \
	.gpr0 = RAX, \
	.fpr0 = XMM0, \
	.rglob = BIT(RBP) | BIT(RSP), \
	.nrglob = 2, \
	.abi0 = elimsb, \
	.memargs = amd64_memargs, \
	.isel = amd64_isel, \

#define AMD64_SYSV_COMMON \
	.ngpr = SYSVNGPR, \
	.nfpr = SYSVNFPR, \
	.rsave = amd64_sysv_rsave, \
	.nrsave = {SYSVNGPS, SYSVNFPS}, \
	.rclob = amd64_sysv_rclob, \
	.nrclob = SYSVNCLR, \
	.retregs = amd64_sysv_retregs, \
	.argregs = amd64_sysv_argregs, \
	.abi1 = amd64_sysv_abi, \
	.emitfn = amd64_sysv_emitfn, \
	AMD64_COMMON

Target T_amd64_sysv = {
	.name = "amd64_sysv",
	.emitfin = elf_emitfin,
	.asloc = ".L",
	AMD64_SYSV_COMMON
};

Target T_amd64_apple = {
	.name = "amd64_apple",
	.apple = 1,
	.emitfin = macho_emitfin,
	.asloc = "L",
	.assym = "_",
	AMD64_SYSV_COMMON
};
